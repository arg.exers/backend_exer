const express = require('express');
const bodyParser = require('body-parser');


module.exports = () => {
    let server = express(),
        create,
        start;


        create = () => {
            const routes = require('./routes/routes');

            server.set('port', process.env.PORT || 80);
            server.set('hostname', process.env.SERVER_IP || '0.0.0.0');
            
            server.use(bodyParser.json());
            server.use('/api', routes.apiRoute);
        }

        start = () => {
            let port = server.get('port'),
                hostName = server.get('hostname');

            server.listen(port ,hostName, () => {
                console.log(`Server is listening on http://${hostName}:${port}`);
            });
        }

        return {
            create,
            start
        };
}
