const redis = require('redis');
const redisURL = require('./config').redisURL;
const redisPort = require('./config').redisPort;
const promisify = require('util').promisify;


let redisClient = redis.createClient(redisPort, redisURL);
let getAsync = promisify(redisClient.get).bind(redisClient);
let setAsync = promisify(redisClient.set).bind(redisClient);


let saveValueToRedis = async (key, value) => {
    try {
        console.log(`Saving ${key} to redis`);
        let res = await setAsync(key, JSON.stringify(value));
	if(res === 'OK') {
            console.log(`${key} successfully saved to redis`);
	}
        return res;
    } catch(err) {
        console.log('Error happened while saving key to redis');
        throw new Error();
    }
};

let getValueFromRedis = async (key) => {
    try {
	console.log(`Trying to get ${key} from redis`);
        let res = await getAsync(key);
        if(res) {
            console.log(`${key} was found.Returning`);
            res = JSON.parse(res);
        } else {
            console.log(`${key} was not found in redis`);
        }
        return res;
    } catch(err) {
        console.log(`Error happened while trying to get the key ${key} from redis`);
        throw new Error();
    }
};


module.exports = {
    saveValueToRedis,
    getValueFromRedis
}
