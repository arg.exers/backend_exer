const express = require('express');
const redis = require('../helpers/redis_wrapper');


let router = express.Router();


router.get('/:resource', (req, res) => {
    if(req.params.resource) {
        let key = req.params.resource;
        redis.getValueFromRedis(key).then(val => {
            if(val) {
                res.status(200).send(val);
            } else {
                res.status(400).send();
            }
        }).catch(err => {
            res.status(500).send();
        });
    } else {
        res.status(400).send();
    }
});

router.post('/:resource', (req, res) => {
    if(req.params.resource) {
        let key = req.params.resource;
        let value = req.body || '';
        redis.saveValueToRedis(key, value).then(val => {
            if(val) {
                res.status(200).send({
                    statusCode: 0,
                    statusMessage: 'OK',
		});
            } else {
                res.status(400).send();
            }
        }).catch(err => {
            res.status(500).send();
        });
    } else {
        res.status(400).send();
    }
});


module.exports = router;
