const expect = require('expect');
const request = require('supertest');
const server = require('../server/server')();


server.create();
server.start();


let port = process.env.PORT || 80;
let hostName = process.env.SERVER_IP || '0.0.0.0';
let resourceObj = { name: 'timur' };


describe('POST /api/resource', () => {
    it('Should save an object with a name resource', (done) => {
        let url = `http://${hostName}:${port}`;
        request(url).post('/api/resource')
        .send(resourceObj)
        .expect(200)
        .expect((res) => {
            expect(res.body).toEqual({
                statusCode: 0,
                statusMessage: 'OK'
            });
        }).end((err, res) => {
            if(err) {
                return done(err);
            }
            done();
        });
    });
});

describe('GET /api/resource', () => {
    it('Should retrieve an object with a name resource', (done) => {
        let url = `http://${hostName}:${port}`;
        request(url).get('/api/resource')
        .send()
        .expect(200)
        .expect((res) => {
            expect(res.body).toEqual(resourceObj);
        }).end((err, res) => {
            if(err) {
                return done(err);
            }
            done();
        });
    });
});


