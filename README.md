# README
## Requirements:
Redis installed on the machine specified inside server/helpers/config.js.

If you wish to work with other redis, just change the url there.

Node 8.14(might work with other versions too).

## How to run:
#### Regular
npm install && npm start

#### Docker
docker build -t timur/exercise .

docker run -p 80:80 -d timur/exercise


## Tests
npm test



Thank you :)