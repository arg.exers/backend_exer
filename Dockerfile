FROM node:8.14.0-jessie
WORKDIR /app
COPY package*.json ./
COPY app.js ./
COPY server ./server
RUN npm install
EXPOSE 80
CMD ["npm", "start"]


